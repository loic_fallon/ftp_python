import socket
import os
import sys
import tqdm
import struct
import time


class ServerFTP:
    def __init__(self, ip='', port=2121, buffer=1024):

        self.__ip = ip
        self.__port = port
        self.__buffer = buffer
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.conn = None
        self.addr = None

        self.data_address = None
        self.datasock = None

        self.mode = 'I'

    def bind(self):
        self.socket.bind((self.__ip, self.__port))
        self.socket.listen(1)
        self.conn, self.addr = self.socket.accept()

    def receive(self):
        return self.conn.recv(self.__buffer)

    def pwd(self):
        path = f'{os.getcwd()}'

        self.conn.send(('257 \"%s\" --> Dossier actuel.\r\n' % path).encode())

    def list_files(self):
        self.conn.send('150\r\n'.encode())
        self.datasock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.datasock.connect(self.data_address)

        dir_list = os.listdir('.')

        for item in dir_list:
            k = self.to_list_item(item)
            self.datasock.send(k.encode())

        self.datasock.close()
        self.datasock = None
        self.conn.send('226 Dossier envoyé.\r\n'.encode())

    def to_list_item(self, fn):
        st = os.stat(fn)
        fullmode = 'rwxrwxrwx'
        mode = ''
        for i in range(9):
            mode += ((st.st_mode >> (8 - i)) & 1) and fullmode[i] or '-'
        d = (os.path.isdir(fn)) and 'd' or '-'
        ftime = time.strftime(' %b %d %H:%M ', time.gmtime(st.st_mtime))
        return d + mode + ' 1 user group ' + str(st.st_size) + ftime + os.path.basename(fn) + '\r\n'

    def port(self, data):

        cmd_addr = data.split(" ")
        cmd_ip_port = cmd_addr[1].split(",")

        ip = ".".join(str(x) for x in cmd_ip_port[0:4])
        port = cmd_ip_port[-2:]
        port = int(port[0]) * 256 + int(port[1])

        server.data_address = (ip, port)

        send = '200  Port command successfull.\r\n'
        server.conn.send(send.encode())

    def download(self, data):
        filename = data.split(" ")[1]

        print('Download file... ', filename)

        try:
            filesize = os.path.getsize(filename)
        except:
            self.conn.send(("550 Fichier inacessible '{}'.\r\n").format(filename).encode())
            return

        self.conn.send('150 Connexion.\r\n'.encode())

        readmode = 'rb'

        f = open(filename, readmode)

        self.datasock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.datasock.connect(self.data_address)

        # self.conn.send(struct.pack("i", filesize))

        while True:
            file = f.read(self.__buffer)
            if not file:
                break
            self.datasock.send(file)

        f.close()
        self.datasock.close()
        self.conn.send('226 Transfert réussi.\r\n'.encode())



    def upload(self, data):

        filename = data.split(" ")[1]
        print(filename)

        self.conn.send('150 Connexion.\r\n'.encode())

        if self.mode != 'I':
            self.datasock.accept()
        else:
            self.datasock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.datasock.connect(self.data_address)

        readmode = 'wb' if self.mode == 'I' else 'w'

        try:
            f = open(filename, readmode)

            i = 0
            while True:
                bytes_recieved = self.datasock.recv(1024)

                if not bytes_recieved:
                    break

                i += 1
                print(i)
                f.write(bytes_recieved)

            f.close()
            self.datasock.close()
            self.conn.send('226 Transfère réussi.\r\n'.encode())
        except:
            self.conn.send("550 Fichier inacessible .\r\n".encode())

    def chdir(self, data):
        pathname = data.split(" ")[1]

        try:
            os.chdir(pathname)
            self.conn.send(('250 \"%s\" --> Dossier actuel.\r\n' % os.getcwd()).encode())

        except:
            print(sys.exc_info())
            self.conn.send(('550 \"{}\" Problème de fichier.\r\n'.format(os.getcwd() + "/" + str(pathname))).encode())

    def delete(self, data):
        filename = data.split(" ")[1]
        os.remove(filename)
        self.conn.send('250 fichier supprimé !\r\n'.encode())

    def welcome_message(self):
        send = '220 Connexion.\r\n'
        self.conn.send(send.encode())

    def _type(self, data):
        self.mode = data.split(" ")[1]
        send = '200 Succès.\r\n'
        self.conn.send(send.encode())

    def pasv(self):
        send = '227 Mode Passif.\r\n'
        self.conn.send(send.encode())

    def abor(self):
        send = '225 Connexion ouverte.\r\n'
        self.conn.send(send.encode())

    def user(self, data):
        user = data.split(" ")[1]
        self.conn.send(('331 OK - {}.\r\n'.format(user)).encode())

    def _pass(self, data):
        self.conn.send('230 OK.\r\n'.encode())

    def quit(self):
        self.conn.send('221 Goodbye.\r\n'.encode())
        self.conn.close()
        self.socket.close()

        os.execl(sys.executable, sys.executable, *sys.argv)


server = ServerFTP()

print('FTP Server Connected')

server.bind()

server.welcome_message()

while True:

    data = server.receive()

    if not data:
        send = '220 Connexion.\r\n'
        server.conn.send(send.encode())

    data = data.decode()

    print("Commande: {0}\n".format(data))

    data_arr = data.split('\r\n')[:-1]

    for i in range(0, len(data_arr)):

        data = data_arr[i]

        if data == "PWD":
            server.pwd()
        elif data == "LIST":
            server.list_files()
        elif "PORT" in data:
            server.port(data)
        elif "CWD" in data:
            server.chdir(data)
        elif "USER" in data:
            server.user(data)
        elif "PASS" in data:
            server._pass(data)
        elif "TYPE" in data:
            server._type(data)
        elif "RETR" in data:
            server.download(data)
        elif "STOR" in data:
            server.upload(data)
        elif "ABOR" in data:
            server.abor()
        elif "DELE" in data:
            server.delete(data)
        elif data == 'PASV':
            server.pasv()
        elif data == "QUIT":
            server.quit()
            break
        else:
            send = '220 Connexion.\r\n'
            server.conn.send(send.encode())

        data = None

# https://fr.wikipedia.org/wiki/Liste_des_codes_des_réponses_d%27un_serveur_FTP
# https://fr.wikipedia.org/wiki/Liste_des_commandes_ftp